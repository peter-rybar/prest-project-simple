// import * as log4js from "log4js";
// import { basename, extname } from "path";
import { Request, Response, NextFunction } from "express";

// const log = log4js.getLogger(basename(__filename, extname(__filename)));

export const absUrl = (req: Request, res: Response, next: NextFunction) => {
    req.absBaseUrl = req.protocol + "://" + req.get("host");
    req.absUrl =  req.absBaseUrl + req.originalUrl;
    // log.debug("absUrl: req.absBaseUrl=%s, req.absUrl=%s",
    //     req.absBaseUrl, req.absUrl);
    return next();
};
