import { Request } from "express";
import * as jwt from "jsonwebtoken";
import * as basicAuth from "basic-auth";
import { Conf, confDefault } from "../conf";

let confAuth: Conf["auth"] = confDefault.auth;

export function initAuthentication(conf: Conf["auth"]): void {
    confAuth = conf;
}

export function expressAuthentication(req: Request, securityName: string, scopes?: string[]): Promise<any> {
    // console.log("expressAuthentication", securityName, scopes, req.auth);

    if ((req as any).user) {
        return Promise.resolve((req as any).user);
    } else {

        switch (securityName) {

            // case "api_key": {
            //     const token = request.query && request.query.access_token;
            //     const apiToken = "abc123456";
            //     if (token === apiToken) {
            //         return Promise.resolve({
            //             id: 1,
            //             name: "Ironman"
            //         });
            //     } else {
            //         return Promise.reject(new Error("Invalid API key"));
            //     }
            // }

            case "basic_auth": {
                const credentials = basicAuth(req);
                if (credentials) {
                    const passwd = confAuth.basic.users[credentials.name];
                    if (credentials.pass === passwd) {
                        return Promise.resolve({
                            username: credentials.name,
                        });
                    } else {
                        return Promise.reject(new Error("Invalid basic auth credentials"));
                    }
                } else {
                    return Promise.reject(new Error("No basic auth credentials"));
                }
            }

            case "jwt": {
                const token = req.body.token || req.query.token || req.headers["x-access-token"];
                return new Promise((resolve, reject) => {
                    if (!token) {
                        reject(new Error("No JWT token"));
                    }
                    jwt.verify(token, confAuth.jwt.secretOrPublicKey, function (err: any, decoded: any) {
                        if (err) {
                            reject(err);
                        } else {
                            if (scopes) {
                                // Check if JWT contains all required scopes
                                for (const scope of scopes) {
                                    if (!decoded.scopes.includes(scope)) {
                                        reject(new Error("JWT does not contain required scope."));
                                    }
                                }
                                resolve(decoded);
                            }
                        }
                    });
                });
            }

            default: {
                return Promise.reject(new Error("Unsupported authentication"));
            }
        }
    }
}
