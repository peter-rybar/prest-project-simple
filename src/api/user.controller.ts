import { Tags, Route, Controller, Request, Security, Get, Post, Body } from "tsoa";
import * as express from "express";
import { User } from "../model/user";

@Route("/user")
@Tags("User")
@Security("jwt")
@Security("basic_auth")
export class UserController extends Controller {

    @Get("/")
    async find(@Request() req: express.Request): Promise<User[]> {
        return req.app.logic.user().find();
    }

    @Post("/")
    async insert(
        @Request() req: express.Request,
        @Body() user: User
    ): Promise<any> {
        return req.app.logic.user().insert(user);
    }

}
