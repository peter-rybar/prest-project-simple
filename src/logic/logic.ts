import { DbStore } from "../store/dbstore";
import { UserLogic } from "./userlogic";
import { Mailer, Message } from "../mail/mailer";

export class Logic {

    private _dbStore: DbStore;
    private _mailer: Mailer;
    private _users: UserLogic;

    constructor(dbStore: DbStore,
                mailer: Mailer,
                users: UserLogic) {
        this._dbStore = dbStore;
        this._mailer = mailer;
        this._users = users;
    }

    close(): Promise<void> {
        return this._dbStore.close();
    }

    dbStore() {
        return this._dbStore;
    }

    user(): UserLogic {
        return this._users;
    }

    mailer(): Mailer {
        return this._mailer;
    }

    mailMessage(message: Message): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const m = this._mailer;
            m.openTransport();
            m.sendMessage(message)
                .then(res => {
                    resolve(res);
                    m.closeTransport();
                })
                .catch(err => {
                    reject(err);
                    m.closeTransport();
                });
        });
    }

}
